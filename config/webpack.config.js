require('../runner/prepare')
const paths = require('./paths')
const constants = require('./constants')
const { webpackAliases } = require('./aliases')

let babelOptions
babelOptions = require('./babel.config')

const mode = process.env.NODE_ENV === 'development' ? constants.MODE.DEVELOPMENT : constants.MODE.PRODUCTION

module.exports = {
  mode,
  devtool: mode === constants.MODE.DEVELOPMENT ? 'eval-source-map' : undefined,
  entry: paths.entry,
  output: {
    filename: 'app.js',
    path: paths.dist,
    publicPath: '/'
  },
  resolve: {
    extensions: ['.mjs', '.js', '.jsx'],
    alias: webpackAliases
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: babelOptions
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|svg|jpg|gif|woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader']
      }
    ]
  },
  node: {
    fs: 'empty'
  }
}