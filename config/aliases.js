import fs from 'fs'
import path from 'path'
import * as paths from './paths'
import { buildJSConfigAliases } from '../helpers/aliases'

const aliases = {
  root: paths.root,
  config: paths.config,
  data: paths.data,
  schema: paths.schema,
  models: paths.models,
  services: paths.services,
  api: paths.api,
  '~': paths.source,
  server: paths.server,
  client: paths.client,
  App: paths.App,
  components: paths.components,
  pages: paths.pages
}

aliases[Symbol.iterator] = () => {
  let current = 0
  const pairs = Object.keys(aliases).map(alias => ({
    alias,
    path: aliases[alias]
  }))

  return {
    next: () => ({
      done: current === pairs.length,
      value: pairs[current++]
    })
  }
}

export const webpackAliases = aliases
export const jsconfigAliases = buildJSConfigAliases(aliases)

const jsConfigFilePath = path.resolve(paths.root, 'jsconfig.json')
const jsConfigFileContent = JSON.parse(fs.readFileSync(jsConfigFilePath, 'utf8'))
jsConfigFileContent.compilerOptions.paths = jsconfigAliases
// console.log(jsConfigFileContent.compilerOptions.paths)
fs.writeFileSync(jsConfigFilePath, JSON.stringify(jsConfigFileContent, null, 2))