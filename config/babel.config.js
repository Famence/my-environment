module.exports = {
  presets: ['@babel/env', '@babel/react'],
  plugins: [
    ['relay', { 'artifactDirectory': './__generated__/relay/' }],
    '@babel/plugin-transform-runtime',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-export-default-from',
    '@babel/plugin-proposal-export-namespace-from',
    'babel-plugin-add-module-exports',
    'react-hot-loader/babel',
    '@babel/plugin-syntax-dynamic-import'
  ]
}