import { getConfiguration } from 'root/helpers/configuration'

export const { SECRET_KEY } = process.env
if (!SECRET_KEY) throw new Error('Please provide a SECRET_KEY to .env file')

export default {
  host: getConfiguration('serverHost', 'localhost'),
  port: getConfiguration('serverPort', '3000'),
  SECRET_KEY
}