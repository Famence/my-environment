import fs from 'fs'
import path from 'path'
import { getConfiguration } from 'root/helpers/configuration'
import * as paths from 'config/paths'
import serverConfig from 'config/server.config'

export default {
  contentBase: [paths.publicFolder],
  compress: false,
  hot: true,
  publicPath: '/',
  historyApiFallback: true,
  overlay: true,
  proxy: {
    '*': {
      target: `https://${serverConfig.host}:${serverConfig.port}`,
      secure: false
    }
  },
  https: getConfiguration('devServerHTTPS', 'false') === 'true' ? {
    key: fs.readFileSync(path.resolve(paths.root, 'ssl', 'localhost.key')),
    cert: fs.readFileSync(path.resolve(paths.root, 'ssl', 'localhost.crt'))
  } : false,
  host: serverConfig.host,
  port: getConfiguration('devServerPort', '8080')
}