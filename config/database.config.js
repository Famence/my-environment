import { getConfiguration } from 'root/helpers/configuration'

export default {
  host: getConfiguration('dbHost', 'localhost'),
  port: getConfiguration('dbPort', '27017'),
  dbName: getConfiguration('dbName'),
  user: getConfiguration('dbUserName'),
  pass: getConfiguration('dbUserPassword')
}