'use strict'

const offlineImageUrl = '/offlineImage.svg'

const config = {
  staticCacheItems: [
    '/app.js',
    offlineImageUrl
  ],
  cachePathPattern: /\.(html|js|css|png|jpg|svg)$/
}

const addToCache = async (cacheKey, request, response) => {
  if (response.ok) {
    const responseCopy = response.clone()
    const cache = await caches.open(cacheKey)
    await cache.put(request, responseCopy)
  }
  return response
}

const fetchFromCache = async event => {
  const response = await caches.match(event.request)
  if (!response) throw Error(`${event.request.url} not found in cache`)
  return response
}

const offlineResponse = resourceType => {
  switch (resourceType) {
    case 'image':
      return fetch(offlineImageUrl)
    default:
      return undefined
  }
}

self.addEventListener('install', event => {
  const onInstall = async (event, options) => {
    const cache = await caches.open('static')
    await cache.addAll(options.staticCacheItems)
  }

  event.waitUntil(
    onInstall(event, config)
  )
})

self.addEventListener('activate', event => {
  console.log('service worker activated')
})

self.addEventListener('fetch', event => {
  const shouldHandleFetch = (event, options) => {
    const request = event.request
    const url = new URL(request.url)
    const criteria = {
      matchesPathPattern: !!(options.cachePathPattern.exec(url.pathname)),
      isGETRequest: request.method === 'GET',
      isFromMyOrigin: url.origin === self.location.origin
    }
    const failingCriteria = Object.keys(criteria).filter(criteriaKey => !criteria[criteriaKey])
    return !failingCriteria.length
  }

  const onFetch = (event, options) => {
    const request = event.request
    const acceptHeader = request.headers.get('Accept')
    let resourceType = 'static'

    if (acceptHeader.indexOf('text/html') !== -1) {
      resourceType = 'content'
    } else if (acceptHeader.indexOf('image') !== -1) {
      resourceType = 'image'
    }

    const cacheKey = resourceType

    if (resourceType === 'content') {
      event.respondWith(
        fetch(request)
          .then(response => addToCache(cacheKey, request, response))
          .catch(() => fetchFromCache(event))
          .catch(() => offlineResponse(resourceType, options))
      )
    } else {
      event.respondWith(
        fetchFromCache(event)
          .catch(() => fetch(request))
          .then(response => addToCache(cacheKey, request, response))
          .catch(() => offlineResponse(resourceType, options))
      )
    }
  }

  shouldHandleFetch(event, config) && onFetch(event, config)
})