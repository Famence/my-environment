const fs = require('fs')
const path = require('path')
const paths = require('../config/paths')

const buildJSConfigAliases = aliases => {
  const jsconfigAliases = {}

  for (let { alias, path: aliasPath } of aliases) {
    const aliasPathExists = fs.existsSync(aliasPath)
    if (!aliasPathExists) continue
    const aliasPathStats = fs.lstatSync(aliasPath)
    const isDirectory = aliasPathStats.isDirectory()

    if (isDirectory) {
      const mainFile = findMainFile(aliasPath)
      if (mainFile) jsconfigAliases[alias] = [relativePath(mainFile)]
      jsconfigAliases[`${alias}/*`] = [`${relativePath(aliasPath)}/*`]
    } else {
      jsconfigAliases[alias] = [relativePath(aliasPath)]
    }
  }

  return jsconfigAliases
}

function relativePath(pathToBeRelative) {
  return `./${path.relative(paths.root, pathToBeRelative)}`
}

function findMainFile(directoryPath) {
  const directoryPathParsed = path.parse(directoryPath)
  const extensionVariants = ['.js', '.jsx']

  for (let index in extensionVariants) {
    const probablyMainFile = path.resolve(directoryPath, `${directoryPathParsed.name}${extensionVariants[index]}`)
    if (fs.existsSync(probablyMainFile)) return probablyMainFile
  }

  return null
}

module.exports = {
  buildJSConfigAliases
}