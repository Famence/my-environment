require('dotenv').config()

function getConfiguration(path, defaultValue) {
  if (defaultValue !== undefined) {
    if (typeof defaultValue !== 'string') throw new Error('defaultValue must be a string as any value in process.env is always string')
  }
  return process.env[path] || defaultValue
}

module.exports = { getConfiguration }