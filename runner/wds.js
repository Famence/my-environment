import wdsOptions from 'config/devServer.config'
import webpackConfig from 'config/webpack.config'
import webpack from 'webpack'
import WebpackDevServer from 'webpack-dev-server'

const devServer = new WebpackDevServer(webpack(webpackConfig), wdsOptions)

devServer.listen(wdsOptions.port, wdsOptions.host, error => {
  if (error) throw error
  console.log(`Webpack Dev Server started at ${wdsOptions.https ? 'https' : 'http' }://${wdsOptions.host}:${wdsOptions.port}`)
})