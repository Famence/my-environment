import dbConfig from 'config/database.config'
import mongoose from 'mongoose'

const { host, port, dbName, user, pass } = dbConfig

const connectionURL = `mongodb://${host}:${port}/${dbName}`
const options = {
  useNewUrlParser: true,
  dbName,
  user,
  pass
}

module.exports = mongoose.connect(connectionURL, options)