require('dotenv').config()
require('@babel/register')(require('../config/babel.config'))
const moduleAlias = require('module-alias')

const { webpackAliases } = require('../config/aliases')
moduleAlias.addAliases(webpackAliases)