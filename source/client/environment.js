import { installRelayDevTools } from 'relay-devtools'
import { Environment, Network, RecordSource, Store } from 'relay-runtime'
import RelayNetworkLogger from 'relay-runtime/lib/RelayNetworkLogger'
import fetchQuery from './fetchQuery'

const __DEV__ = process.env.NODE_ENV === 'development'
if (__DEV__) installRelayDevTools()

const network = Network.create(__DEV__ ? RelayNetworkLogger.wrapFetch(fetchQuery) : fetchQuery)

const source = new RecordSource()
export const store = new Store(source)

const environment = new Environment({
  network,
  store
})

export default environment