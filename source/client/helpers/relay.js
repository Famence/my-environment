import environment from 'client/environment'
import * as React from 'react'
import { QueryRenderer } from 'react-relay'

export const attachQueryRenderer = (Component, config) => {
  const { query, queriesParams, onError } = config

  return componentProps => {
    const variables = queriesParams ? queriesParams(componentProps) : config.variables

    return (
      <QueryRenderer
        environment={environment}
        query={query}
        variables={variables}
        render={({ error, props: queryResponse }) => {
          if (error) {
            if (typeof onError === 'function') return onError(error)
            console.error(error)
            return <span>Error fetching data</span>
          }
          return (
            <Component {...componentProps} {...queryResponse} isFetching={!queryResponse} />
          )
        }}
      />
    )
  }
}