import BlockLoadingSpinner from 'components/BlockLoadingSpinner/BlockLoadingSpinner'
import React from 'react'

export const withLoadingSpinner = Component => props => (
  <BlockLoadingSpinner>
    <Component {...props} />
  </BlockLoadingSpinner>
)