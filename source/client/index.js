require('dotenv').config()
import Root from 'App/Root'
import * as serviceWorker from 'client/serviceWorker'
import { createBrowserHistory } from 'history'
import React from 'react'
import ReactDOM from 'react-dom'
import moment from 'moment'

moment.locale('ru')

export const browserHistory = createBrowserHistory()

const rootElement = document.getElementById('root')
if (rootElement) {
  ReactDOM.render(
    <Root />,
    rootElement
  )
}

serviceWorker.unregister() // Change to .register() to start working on it