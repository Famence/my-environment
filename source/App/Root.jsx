import App from 'App/App'
import { browserHistory } from 'client'
import React from 'react'
import { hot } from 'react-hot-loader/root'
import { Router } from 'react-router-dom'
import { css, Global } from '@emotion/core'
import { accentFont, colors, contentFont } from './commonStyles'

const Root = () => (
  <Router history={browserHistory}>
    <Global styles={globalStyles} />
    <App />
  </Router>
)

export default hot(Root)

const globalStyles = css`
@import url('https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i&display=swap');

html {
  background: #ECECF0;
}

body {
  margin: 0;
  ${contentFont};
}

h1, h2, h3, h4 {
  ${accentFont};
}
h3 {
  font-weight: 800;
}

button {
  display: block;
  margin: 1em auto;
  padding: .8em 2em .9em;
  font-size: 1.1em;
  border: none;
  background: ${colors.accent};
  font-family: 'Montserrat', sans-serif;
  font-weight: 700;
  &:focus {
    outline: none;
    box-shadow: 0 .4em .9em rgba(0,0,0,.2)
  }
}
`