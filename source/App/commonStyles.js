import { css } from '@emotion/core'

export const colors = {
  accent: '#FED80A'
}

export const horizontalPaddings = css`
  padding-right: 10px;
  padding-left: 10px;
`

export const accentFont = css`
  font-family: 'Montserrat', sans-serif;
`
export const contentFont = css`
  font-family: 'Roboto', sans-serif;
`