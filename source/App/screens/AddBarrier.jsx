/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import React from 'react'
import AddBarrierForm from 'components/AddBarrierForm'
import { colors } from 'App/commonStyles'

export default () => (
  <div css={styles.wrapper}>
    <h1 css={styles.heading}>Сделаем городскую среду доступной вместе!</h1>
    <AddBarrierForm />
  </div>
)

const styles = {
  wrapper: css`
    min-height: 100vh;
    width: 100vw;
    box-sizing: border-box;
    background: #000;
    padding: 40px 15px;
  `,
  heading: css`
    color: ${colors.accent};
    font-size: 1.9em;
    margin: 0 0 1em;
    padding: 0 10px;
`
}