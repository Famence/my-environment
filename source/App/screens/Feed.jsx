import React from 'react'
import BarriersList from 'components/BarriersList/BarriersList'
import BottomPanel from 'components/BottomPanel/BottomPanel'

export default () => (
  <>
    <BarriersList />
    <BottomPanel />
  </>
)