import React from 'react'
import { Route, Router, Switch } from 'react-router-dom'
import { browserHistory } from 'client'

import Feed from 'App/screens/Feed'
import AddBarrier from 'App/screens/AddBarrier'
import BarrierScreen from 'App/screens/BarrierScreen'

class App extends React.Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Switch>
          <Route exact path="/" component={Feed} />
          <Route exact path="/add" component={AddBarrier} />
          <Route exact path="/barrier/:barrierId" component={BarrierScreen} />
        </Switch>
      </Router>
    )
  }
}

export default App