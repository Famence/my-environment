import { css } from '@emotion/core'
import { accentFont, contentFont, colors } from 'App/commonStyles'

export const labelWrapper = css`
    display: flex;
    flex-flow: column nowrap;
`

export const labelText = css`
    ${accentFont};
    font-weight: 600;
    font-size: 1.1em;
    padding: 0 .3em;
    margin-bottom: .5em;
`

export const textable = css`
    border: none;
    background: none;
    border-bottom: 1px solid rgba(0,0,0,.4);
    font-size: 1em;
    ${contentFont};
    padding: .3em;
    &:focus {
      outline: none;
      border-color: ${colors.accent}
    }
`

export const input = css`
    ${textable};
`

export const textarea = css`
    ${textable};
`