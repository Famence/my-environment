/** @jsx jsx */
import { jsx } from '@emotion/core'
import * as React from 'react'
import * as controlsStyles from './controlsStyles'

class InputControl extends React.Component {
  onChange = event => {
    this.props.onChange && this.props.onChange(event.target.value, event)
  }

  render() {
    let { label, ...props } = this.props
    if (props.placeholder === undefined) props.placeholder = label

    props.onChange = this.onChange

    return (
      <label css={controlsStyles.labelWrapper}>
        <span css={controlsStyles.labelText}>{label}</span>
        <textarea {...props} css={controlsStyles.textarea} />
      </label>
    )
  }
}

export default InputControl