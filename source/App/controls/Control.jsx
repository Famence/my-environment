/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import CONTROL_TYPES from 'App/controls/controlTypes'
import InputControl from 'App/controls/Input'
import TextareaControl from 'App/controls/Textarea'
import React from 'react'

const renderControl = ({ type, ...props }) => {
  switch (type) {
    case CONTROL_TYPES.INPUT:
      return <InputControl {...props} />
    case CONTROL_TYPES.PASSWORD:
      return <InputControl {...props} type='password' />
    case CONTROL_TYPES.TEXTAREA:
      return (
        <TextareaControl {...props} />
      )
  }
}

const Control = ({ style, ...props }) => (
  <div css={[styles.wrapper, style]}>
    {renderControl(props)}
  </div>
)

export default Control

const styles = {
  wrapper: css`
    margin-top: 2.7em;
`
}