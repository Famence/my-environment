/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import React, { Component } from 'react'
import Control from 'App/controls/Control'
import CONTROL_TYPES from 'App/controls/controlTypes'
import { colors } from 'App/commonStyles'
import environment from '../../client/environment'
import { commitMutation, graphql } from 'react-relay'

const addBarrierMutation = graphql`
  mutation AddBarrierFormMutation($title: String, $description: String!, $address: String!) {
    addBarrier(title: $title, description: $description, address: $address) {
      status
    }
  }
`

class AddBarrierForm extends Component {
  state = {
    title: '',
    description: '',
    address: ''
  }

  render() {
    const { title, description, address } = this.state
    return (
      <form css={styles.wrapper} onSubmit={this.onSubmit}>
        <h2 css={styles.formHeading}>Что у вас?</h2>
        <Control
          label={(
            <>
              Придумайте заголовок<br />
              <span css={css`font-size: .7em; opacity: .6;`}>(не обязательно)</span>
            </>
          )}
          type={CONTROL_TYPES.INPUT}
          placeholder='Перекопан тротуар'
          value={title}
          onChange={this.onChangeTitle}
        />
        <Control
          label='Опишите проблему'
          type={CONTROL_TYPES.TEXTAREA}
          onChange={this.onChangeDescription}
          placeholder='Уже полгода обходим по газону эту часть тротуара. Раскопали-закопали, а тротуар не восстановили.' // Пожалуйста, опишите какой объект понижает доступность или качество городской среды. Его местоположение. Кому он может доставить неудобства? Если у вас есть идеи как это исправить, тоже пишите.
          value={description}
          rows={3}
        />
        <Control
          label='Укажите адрес или метку на карте'
          type={CONTROL_TYPES.INPUT}
          placeholder='Екатеринбург, ЖК Жи-Ка, возле пятёрочки'
          value={address}
          onChange={this.onChangeAddress}
        />
        <button css={styles.sendButton}>Отправить</button>
      </form>
    )
  }

  onChangeTitle = title => {
    this.setState({ title })
  }

  onChangeDescription = description => {
    this.setState({ description })
  }

  onChangeAddress = address => {
    this.setState({ address })
  }

  onSubmit = event => {
    event.preventDefault()

    const { title, description, address } = this.state
    commitMutation(
      environment, // TODO: Желательно избавиться от необходимости постоянно передавать environment
      {
        mutation: addBarrierMutation,
        variables: { title, description, address },
        optimisticResponse: {
          addBarrier: {
            status: 'ok'
          }
        },
        onCompleted: (response, errors) => {
          console.log(response, errors)
          if (response.status === 'ok') console.log('OK!')
        },
        onError: console.error
      }
    )
  }
}

export default AddBarrierForm

const styles = {
  wrapper: css`
    border-radius: 30px;
    background: #fff;
    padding: 30px 15px;
`,
  formHeading: css`
    margin-top: 0;
    text-align: center;
`,
  sendButton: css`
    font-size: 1.2em;
    border: 0;
    background: ${colors.accent};
    border-radius: .5em;
    margin: 2em auto 0;
`
}