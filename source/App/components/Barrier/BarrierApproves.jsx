/** @jsx jsx */
import { css, jsx } from '@emotion/core'
import React from 'react'
import * as ApproveIcons from 'App/icons/Approve'

const BarrierApproves = ({ count, approvedByCurrentUser }) => (
  <div css={styles.wrapper}>
    {approvedByCurrentUser ? <ApproveIcons.Colored css={styles.icon} /> : <ApproveIcons.Linear css={styles.icon} />}
    <div css={[styles.count, approvedByCurrentUser && styles.countApprovedByCurrentUser]}>
      {count}
    </div>
  </div>
)

export default BarrierApproves

const styles = {
  wrapper: css`
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
`,
  icon: css`
    margin-right: 6px;
    height: 23px;
`,
  count: css`
    font-family: 'Montserrat', sans-serif;
    font-size: 1.2em;
    color: #3b2391;
    font-weight: 400;
`,
  countApprovedByCurrentUser: css`
    font-weight: 600;
`
}