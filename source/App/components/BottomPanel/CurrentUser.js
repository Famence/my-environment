/** @jsx jsx */
import { css, jsx } from '@emotion/core'
import React, { Component } from 'react'
import { graphql } from 'react-relay'
import { attachQueryRenderer } from 'client/helpers/relay'
import LoadingBlock from 'components/LoadingBlock'
import UserImage from 'components/User/UserImage'
import { accentFont, horizontalPaddings } from 'App/commonStyles'

class CurrentUser extends Component {
  render() {
    let { isFetching, currentUser } = this.props
    if (isFetching) return <LoadingBlock />

    return (
      <div css={styles.wrapper}>
        {currentUser ? this.renderAuthorized() : this.renderUnauthorized()}
      </div>
    )
  }

  renderAuthorized = () => {
    let { currentUser } = this.props
    return (
      <>
        <div css={styles.imageHolder}>
          <UserImage userId={currentUser.id} />
        </div>
        <div>
          <span css={styles.userName}>{currentUser.name}</span><br />
          {this.renderUserStats()}
        </div>
      </>
    )
  }

  renderUserStats = () => {
    return (
      <span css={styles.stats}>
        Обнаружил <b css={styles.statsAccentNumber}>13</b> барьеров.<br />
        Из них <b css={styles.statsAccentNumber}>5</b> уже устранены.
      </span>
    )
  }

  renderUnauthorized = () => (
    'Unauthorized'
  )
}

export default attachQueryRenderer(CurrentUser, {
  query: graphql`
    query CurrentUserQuery {
      currentUser: user {
        id
        name
      }
    }
  `
})

const styles = {
  wrapper: css`
    width: 100%;
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
`,
  imageHolder: css`
    margin-right: 10px;
`,
  userName: css`
    display: inline-block;
    border-radius: .2em;
    background: #000;
    color: #fff;
    padding: .1em .4em;
    ${accentFont};
    font-weight: 500;
    font-size: .85em;
`,
  stats: css`
    display: inline-block;
    font-size: .8em;
    line-height: 1.2em;
`,
  statsAccentNumber: css`
    ${accentFont};
    font-weight: 500;
    text-decoration: underline;
`
}