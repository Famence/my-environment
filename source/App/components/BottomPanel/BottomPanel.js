/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import React, { Component } from 'react'
import CurrentUser from './CurrentUser'
import AddLocationIcon from 'App/icons/AddLocation'
import { horizontalPaddings, colors } from 'App/commonStyles'
import { browserHistory } from 'client'

class BottomPanel extends Component {
  state = {
    height: 0,
    addBarrierFormOpened: false
  }

  wrapper = React.createRef()

  componentDidMount() {
    this.setState({
      height: this.wrapper.current.offsetHeight
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.height === this.wrapper.current.offsetHeight) return;
    this.setState({
      height: this.wrapper.current.offsetHeight
    })
  }

  render() {
    const { height, addBarrierFormOpened } = this.state
    if (addBarrierFormOpened) {
      setTimeout(() => browserHistory.push('/add'), 200)
    }

    return (
      <>
        <div css={styles.wrapper} ref={this.wrapper}>
          <CurrentUser />
          <div css={styles.addBarrier}>
            <AddLocationIcon css={[styles.icon, addBarrierFormOpened && styles.iconGrown ]} onClick={this.openAddBarrierForm} />
          </div>
        </div>
        <div css={{ height }} />
      </>
    )
  }

  openAddBarrierForm = () => {
    this.setState({ addBarrierFormOpened: !this.state.addBarrierFormOpened })
  }
}

export default BottomPanel

const styles = {
  wrapper: css`
    position: fixed;
    bottom: 0;
    right: 0;
    left: 0;
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    background: #fff;
    padding: 5px 0;
    ${horizontalPaddings};
    border-top: 1px solid rgba(0,0,0,.1);
    box-shadow: 0px -5px 10px rgba(0,0,0,.1);
`,
  addBarrier: css`
    float: right;
`,
  icon: css`
    height: 40px;
    padding: 5px;
    border-radius: 50%;
    background: ${colors.accent};
    box-shadow: 0 6px 8px rgba(0,0,0,.25);
    transition: all .25s ease-in-out;
    cursor: pointer;
    & > * {
      transition: opacity .1s ease-out .15s, transform .25s ease-in-out;
    }
`,
  iconGrown: css`
    transform: scale(40);
    background: #000;
    & > * {
      opacity: 0;
      transform: scale(-40);
      transition: opacity .1s ease-in, transform .25s ease-in-out;
    }
`
}