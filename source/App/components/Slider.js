/** @jsx jsx */
import { css, jsx } from '@emotion/core'
import React from 'react'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import SlickSlider from "react-slick"
import * as URI from "uri-js"

const sliderSettings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  vertical: false,
  verticalSliding: false,
  adaptiveHeight: false,
  mobileFirst: true,
  arrows: false
}

const Slider = ({ images, urlBase, style }) => (
  <div css={[styles.wrapper, style]}>
    <SlickSlider {...sliderSettings}>
      {images.map(imageName => (
        <img src={URI.resolve(urlBase+'/', imageName)} css={styles.image} alt={imageName} key={imageName} />
      ))}
    </SlickSlider>
  </div>
)

export default Slider

const styles = {
  wrapper: css`
    & .slick-slide {
      & > div {
        position: relative;
        padding-top: 50%;
      }
    }
`,
  image: css`
    position: absolute;
    height: auto;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
    pointer-events: none;
`
}