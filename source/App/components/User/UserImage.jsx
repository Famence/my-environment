/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import React from 'react'

const UserImage = ({ userId }) => (
  <div css={styles.wrapper}>
    <img src={`/images/users/${userId}.jpg`} css={styles.image} />
  </div>
)

export default UserImage

const styles = {
  wrapper: css`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    overflow: hidden;`,
  image: css`
    display: block;
    width: 100%;
    height: 100%;`
}