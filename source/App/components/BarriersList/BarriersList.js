/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import React from 'react'
import BarrierListItem from './BarrierListItem'
import { attachQueryRenderer } from 'client/helpers/relay'
import { graphql } from 'react-relay'
import LoadingBlock from 'components/LoadingBlock'

const BarriersList = ({ isFetching, barriers }) => {
  if (isFetching) return <LoadingBlock />

  return (
    <div css={styles}>
      {barriers.map(barrier => <BarrierListItem key={barrier.id} barrier={barrier} />)}
    </div>
  )
}

export default attachQueryRenderer(BarriersList, {
  query: graphql`
    query BarriersListQuery {
      barriers(range: [0, 20]) {
        id,
        title,
        description,
        address,
        location {
          latitude
          longitude
        },
        photos,
        approves {
          count
          approvedByCurrentUser
        },
        author {
          id
          fullName
        },
        date {
          publish
          resolved
        }
      }
    }
  `
})

const styles = css`
  margin: -20px 0;
`