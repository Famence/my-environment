/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import React, { Component } from 'react'
import { horizontalPaddings } from 'App/commonStyles'
import moment from 'moment'
import Slider from 'components/Slider'
import BarrierApproves from 'components/Barrier/BarrierApproves'
import { accentFont } from 'App/commonStyles'
import { browserHistory } from 'client'

class BarrierListItem extends Component {
  onClick = () => {
    //browserHistory.push(`/barrier/${this.props.barrier.id}`)
  }

  render() {
    return (
      <div css={styles.wrapper} onClick={this.onClick}>
        {this.renderHeader()}
        {this.renderBody()}
        {this.renderMeta()}
        {this.renderFooter()}
      </div>
    )
  }

  renderHeader = () => {
    const { barrier } = this.props
    return (
      <div css={styles.header}>
        <h3 css={styles.title}>{barrier.title}</h3>
      </div>
    )
  }

  renderBody = () => {
    const { barrier } = this.props
    return (
      <div css={styles.body}>
        {barrier.photos.length && (
          <Slider images={barrier.photos} urlBase={`/images/barrier/${barrier.id}`} style={styles.slider} />
        )}
        {barrier.description && (
          <p css={styles.description}>{barrier.description}</p>
        )}
      </div>
    )
  }

  renderMeta = () => {
    const { barrier: { location, approves } } = this.props
    return (
      <div css={styles.meta}>
        <BarrierApproves count={approves.count} approvedByCurrentUser={approves.approvedByCurrentUser} />
        <div css={styles.metaRight}>
          {/*{location.latitude && location.longitude && (*/}
          {/*  <span css={styles.metaMapLink}>Показать на карте</span>*/}
          {/*)}*/}
        </div>
      </div>
    )
  }

  renderFooter = () => {
    const { barrier: { author, date } } = this.props
    return (
      <div css={styles.footer}>
        {/*<UserImage userId={author.id} />*/}
        <div css={styles.footerContent}>
          <div>
            <span css={styles.author}>{author.fullName}</span>
            {` ${moment(date.publish).fromNow()}`}
          </div>
          {/*{date.resolved && (*/}
          {/*  <div>*/}
          {/*    Исправлено&nbsp;*/}
          {/*    {moment(date.resolved).fromNow()}*/}
          {/*  </div>*/}
          {/*)}*/}
        </div>
      </div>
    )
  }
}

export default BarrierListItem

const styles = {
  wrapper: css`
    background: #fff;
    box-shadow: 0px 0px 3px rgba(0,0,0,.2);
    margin: 20px 0;
`,
  header: css`
    ${horizontalPaddings};
`,
  title: css`
    margin: 0;
    padding: .7em 0 .5em;
`,
  body: css`
    margin-bottom: 25px;
    ${horizontalPaddings};
`,
  slider: css`
    margin: 0 -10px;
`,
  description: css`
    margin: 18px 0 22px;
`,
  meta: css`
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    ${horizontalPaddings};
`,
  metaRight: css`
    text-align: right;
    width: 100%;
`,
  metaMapLink: css`
    ${accentFont};
    font-weight: 300;
    color: dodgerblue;
    cursor: pointer;
`,
  footer: css`
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    padding: 0 0 19px;
    margin-top: 25px;
    ${horizontalPaddings};
    //background: #faf3ff;
`,
  author: css`
    ${accentFont};
    font-weight: 700;
`,
  footerContent: css`
    //margin-left: 10px;
`
}