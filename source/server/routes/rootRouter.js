import express from 'express'
import * as paths from 'config/paths'
import graphQLHTTP from 'express-graphql'
import { schema } from 'schema/schema'

const rootRouter = express.Router()

rootRouter.use(express.static('public'))
rootRouter.use(express.static('dist'))

rootRouter.use('/graphql', graphQLHTTP({
  schema,
  pretty: true,
  rootValue: root,
  graphiql: process.env.NODE_ENV !== 'production'
}))

rootRouter.get('*', (request, response) => (
  response.sendFile(paths.template)
))

export default rootRouter