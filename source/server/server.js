import https from 'https'
import express from 'express'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import { root } from 'config/paths'
import serverConfig, { SECRET_KEY } from 'config/server.config'
import MongoStore from 'connect-mongo'
import cookieParser from 'cookie-parser'
import session from 'express-session'
import fs from 'fs'
import path from 'path'
import rootRouter from 'server/routes/rootRouter'
import User from 'models/User/UserModel'
import jwt from 'jsonwebtoken'

const app = express()

app.set('trust proxy', 1)

app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(session({
  secret: SECRET_KEY,
  saveUninitialized: false,
  cookie: { secure: true, httpOnly: true },
  resave: false,
  store: new (MongoStore(session))({ mongooseConnection: mongoose.connection })
}))

app.use(async (request, response, next) => {
  const { ssid } = request.cookies
  if (!ssid) return next()
  const { userId } = jwt.verify(ssid, SECRET_KEY)
  request.user = await User.findOne({ _id: userId })
  return next()
})

app.use(async (request, response, next) => {
  await User.auth({ email: 'famence@yandex.ru', password: 'MyPassw0rd' }, { request, response })
  next()
})

app.use(rootRouter)

https.createServer({
  key: fs.readFileSync(path.resolve(root, 'ssl', 'localhost.key')),
  cert: fs.readFileSync(path.resolve(root, 'ssl', 'localhost.crt'))
}, app)
  .listen(serverConfig.port, serverConfig.host, () => (
    console.log(`App listening on https://${serverConfig.host}:${serverConfig.port}`)
  ))
