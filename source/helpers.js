import queryString from 'querystring'

export const buildStaticMapUri = coordinates => {
  if (!coordinates || !coordinates.latitude || !coordinates.longitude) return
  const locationString = `${coordinates.latitude},${coordinates.longitude}`
  return `https://maps.googleapis.com/maps/api/staticmap?${queryString.stringify({
    center: locationString,
    zoom: 14,
    size: '800x500',
    markers: `color:blue|${locationString}`,
    key: 'AIzaSyA32Alf2ilD-r5fNTYXN0vbJ1ZbAhH_I3A'
  })}`
}