import mongoose from 'mongoose'
import User from './User/UserModel'

const { ObjectId } = mongoose.SchemaTypes

export const BarrierSchema = new mongoose.Schema({
  title: {
    type: String
  },
  description: {
    type: String
  },
  address: {
    type: String,
    required: true
  },
  location: {
    latitude: {
      type: Number
    },
    longitude: {
      type: Number
    }
  },
  photos: [String],
  approves: {
    type: [{
      type: ObjectId,
      ref: User
    }]
  },
  date: {
    publish: Date,
    resolved: Date
  },
  author: {
    type: ObjectId,
    ref: User,
    required: true
  }
})

const Barrier = mongoose.model('Barrier', BarrierSchema)

export default Barrier