import mongoose from 'mongoose'

export const RoleSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  }
})

RoleSchema.path('name').validate(function(name) {
  return name.length
}, 'Role name cannot be blank')

const Role = mongoose.model('Role', RoleSchema)
export default Role