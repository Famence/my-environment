import { UserSchema } from 'models/User/UserModel'
import mongoose from 'mongoose'

UserSchema.path('_name.first').validate(function(name) {
  return name.length
}, 'First name cannot be blank')

UserSchema.path('_email.current').validate(function(email) {
  return email.length
}, 'Email cannot be blank')

UserSchema.path('_email.current').validate(function(email) {
  return new Promise(resolve => {
    const User = mongoose.model('User')

    if (this.isNew || this.isModified('_email.current')) {
      User.find({ email }).exec((err, users) => resolve(!err && !users.length))
    } else resolve(true)
  })
}, 'Email `{VALUE}` already exists')

UserSchema.path('_password.current').validate(function(hashedPassword) {
  return !!hashedPassword.length
}, 'Password cannot be blank')