import { UserSchema } from './UserModel'
import Role from '../Role'

UserSchema.pre('save', async function() {
  if (this.roles.length === 0) this.roles = [await Role.findOne({ name: 'user' })]
})