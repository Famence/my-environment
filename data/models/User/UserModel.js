import { SECRET_KEY } from 'config/server.config'
import crypto from 'crypto'
import jwt from 'jsonwebtoken'
import Role from 'models/Role'
import mongoose from 'mongoose'

const { ObjectId } = mongoose.SchemaTypes

export const UserSchema = new mongoose.Schema({
  _name: {
    first: { type: String },
    last: { type: String, default: '' }
  },
  _email: {
    current: { type: String },
    confirmedAt: { type: Date }
  },
  roles: {
    type: [{ type: ObjectId, ref: Role }]
  },
  salt: { type: String },
  _password: {
    current: { type: String },
    lastChanged: { type: Date }
  }
})

UserSchema.virtual('name')
  .get(function() {
    return this._name.first
  })
UserSchema.virtual('lastName')
  .get(function() {
    return this._name.last
  })
UserSchema.virtual('fullName')
  .get(function() {
    return this._name.first + (this._name.last ? ' ' + this._name.last : '')
  })
UserSchema.virtual('password')
  .set(function(password) {
    this.makeSalt() // We generate new salt on every password change
    this._password.current = this.encryptPassword(password)
  })
  .get(function() {
    return this._password.current
  })
UserSchema.virtual('email')
  .set(function(email) {
    if (this.isNew)
      this._email.current = email
  })
  .get(function() {
    return this._email.current
  })


UserSchema.methods = {
  checkPassword: function(plainTextPassword) {
    return this.encryptPassword(plainTextPassword) === this._password.current
  },

  makeSalt: function() {
    this.salt = crypto.randomBytes(128).toString('hex')
  },

  encryptPassword: function(password) {
    if (!password) return ''
    try {
      return crypto
        .createHmac('sha1', this.salt)
        .update(password)
        .digest('hex')
    } catch(err) {
      return ''
    }
  }
}

UserSchema.statics = {
  AUTH_STATUS: {
    OK: 'USER_AUTH_STATUS_OK',
    WRONG_CREDENTIALS: 'USER_AUTH_STATUS_WRONG_CREDENTIALS',
    ALREADY_AUTHORIZED: 'USER_AUTH_STATUS_ALREADY_AUTHORIZED'
  },
  auth: async ({ email, password }, { context, request, response }) => {
    if (context) response = context.res
    if ((context && context.user) || (request && request.user)) return { status: User.AUTH_STATUS.ALREADY_AUTHORIZED }
    const user = await User.findOne({ _email: { current: email } })
    if (!user || !user.checkPassword(password)) return { status: User.AUTH_STATUS.WRONG_CREDENTIALS }
    const ssid = jwt.sign({ userId: user._id }, SECRET_KEY)
    response.cookie('ssid', ssid, {
      httpOnly: true,
      secure: true
    })
    return { status: User.AUTH_STATUS.OK }
  }
}

require('./UserModelValidations')
require('./UserModelHooks')

const User = mongoose.model('User', UserSchema)

export default User