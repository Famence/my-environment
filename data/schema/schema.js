import { SECRET_KEY } from 'config/server.config'
import { GraphQLObjectType, GraphQLSchema } from 'graphql'
import { applyMiddleware } from 'graphql-middleware'
import jwt from 'jsonwebtoken'
import User from 'models/User/UserModel'
import AuthMutation from 'schema/mutations/AuthMutation'
import RoleQuery from 'schema/queries/RoleQuery'
import UserQuery from 'schema/queries/UserQuery'
import BarrierQuery from 'schema/queries/BarrierQuery'
import BarriersQuery from 'schema/queries/BarriersQuery'
import AddBarrierMutation from 'schema/mutations/AddBarrierMutation'

const Query = new GraphQLObjectType({
  name: 'Query',
  fields: {
    user: UserQuery,
    role: RoleQuery,
    barrier: BarrierQuery,
    barriers: BarriersQuery
  }
})

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    auth: AuthMutation,
    addBarrier: AddBarrierMutation
  }
})

/*
This global middleware sets context.user by extracting a user id
from ssid cookie and query for it in users collection.
 */
const userSetterMiddleware = async (resolve, root, args, context, info) => {
  const { ssid } = context.cookies
  if (!ssid) return resolve(root, args, context, info)
  const { userId } = jwt.verify(ssid, SECRET_KEY)
  context.user = await User.findOne({ _id: userId })
  return resolve(root, args, context, info)
}

export const schema = (
  applyMiddleware(
    new GraphQLSchema({
      query: Query,
      mutation: Mutation
    }),
    userSetterMiddleware
  )
)
