import Role from 'models/Role'
import { GraphQLString } from 'graphql'
import { GraphQLRole } from 'schema/types'

const RoleQuery = {
  type: GraphQLRole,
  args: {
    id: { type: GraphQLString }
  },
  resolve: async (root, { id }) => await Role.findOne({ _id: id })
}

export default RoleQuery
