import { GraphQLString } from 'graphql'
import { GraphQLBarrier } from 'schema/types'
import Barrier from 'models/Barrier'

const BarrierQuery = {
  type: GraphQLBarrier,
  args: {
    id: { type: GraphQLString }
  },
  resolve: async (root, { id }) => await Barrier.findOne({ _id: id })
}

export default BarrierQuery
