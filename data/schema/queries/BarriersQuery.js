import { GraphQLInt, GraphQLList, GraphQLNonNull } from 'graphql'
import { GraphQLBarrier } from 'schema/types'
import Barrier from 'models/Barrier'

const BarriersQuery = {
  type: new GraphQLList(GraphQLBarrier),
  args: {
    range: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt))
    }
  },
  resolve: async (root, { range }) => {
    if (range[1] === 0) range[1] = 20
    if (range.length === 0) return null
    if (range.length === 1) return await Barrier.find().limit(range[0])
    return await Barrier.find().sort({ 'date.publish': -1 }).skip(range[0]).limit(range[1])
  }
}

export default BarriersQuery