import { GraphQLString } from 'graphql'
import User from 'models/User/UserModel'
import { GraphQLUser } from 'schema/types'

const UserQuery = {
  type: GraphQLUser,
  args: {
    id: { type: GraphQLString }
  },
  resolve: async (root, { id }, { user }) => {
    if (id) return await User.findOne({ _id: id }) // If user id provided, we just query for user
    return user // else we are trying to return current user
  }
}

export default UserQuery
