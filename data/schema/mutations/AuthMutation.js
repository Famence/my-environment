import { GraphQLNonNull, GraphQLObjectType, GraphQLString } from 'graphql'
import User from 'models/User/UserModel'

const AuthResult = new GraphQLObjectType({
  name: 'AuthResult',
  fields: {
    status: { type: GraphQLString },
    token: { type: GraphQLString }
  }
})

const AuthMutation = {
  type: AuthResult,
  args: {
    email: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve: async (root, credentials, context) => await User.auth(credentials, { context })
}

export default AuthMutation
