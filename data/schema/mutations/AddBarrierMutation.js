import { GraphQLNonNull, GraphQLObjectType, GraphQLString } from 'graphql'
import Barrier from 'models/Barrier'

const AddBarrierMutationResult = new GraphQLObjectType({
  name: 'AddBarrierMutationResult',
  fields: {
    status: { type: GraphQLString },
    token: { type: GraphQLString }
  }
})

const AddBarrierMutation = {
  type: AddBarrierMutationResult,
  args: {
    title: { type: GraphQLString },
    description: { type: new GraphQLNonNull(GraphQLString) },
    address: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve: async (root, data, { user }) => {
    if (!data.title) data.title = `Проблема @ ${data.address}`
    const newBarrier = new Barrier(data)
    newBarrier.approves.push(user)
    newBarrier.author = user
    newBarrier.date.publish = new Date()
    try {
      await newBarrier.save()
      return {
        status: 'ok'
      }
    } catch(error) {
      return {
        status: 'error'
      }
    }
  }
}

export default AddBarrierMutation
