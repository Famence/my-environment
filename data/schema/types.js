import { GraphQLObjectType, GraphQLString, GraphQLList, GraphQLInt, GraphQLFloat, GraphQLBoolean } from 'graphql'
import { GraphQLID } from 'graphql/type/scalars'
import Role from 'models/Role'
import User from 'models/User/UserModel'
import { GraphQLDateTime } from 'graphql-iso-date'
import { buildStaticMapUri } from '~/helpers'

export const GraphQLRole = new GraphQLObjectType({
  name: 'Role',
  fields: {
    id: {
      type: GraphQLID
    },
    name: {
      type: GraphQLString
    }
  }
})

export const GraphQLUser = new GraphQLObjectType({
  name: 'User',
  fields: {
    id: {
      type: GraphQLID
    },
    name: {
      type: GraphQLString
    },
    lastName: {
      type: GraphQLString
    },
    fullName: {
      type: GraphQLString
    },
    roles: {
      type: new GraphQLList(GraphQLRole),
      resolve: async user => await Role.find({ _id: { $in: user.roles } })
    },
    email: {
      type: GraphQLString
    }
  }
})

export const Location = new GraphQLObjectType({
  name: 'Location',
  fields: {
    latitude: {
      type: GraphQLFloat
    },
    longitude: {
      type: GraphQLFloat
    }
  }
})

export const BarrierDate = new GraphQLObjectType({
  name: 'BarrierDate',
  fields: {
    publish: {
      type: GraphQLDateTime
    },
    resolved: {
      type: GraphQLDateTime
    }
  }
})

const Approves = new GraphQLObjectType({
  name: 'Approves',
  fields: {
    count: {
      type: GraphQLInt,
      //resolve: approves => approves.length
      resolve: () => Math.floor((Math.random() * (925 - 12) + 12)) // На время презентации мокаем кол-во подтверждений чтобы выглядело красивее. Потом вернуть оригинальный метод на предыдущей строке
    },
    approvedByCurrentUser: {
      type: GraphQLBoolean,
      resolve: async (approves, barrier, { user }) => (
        approves.some(approve => approve.equals(user._id))
      )
    }
  }
})

export const GraphQLBarrier = new GraphQLObjectType({
  name: 'Barrier',
  fields: {
    id: {
      type: GraphQLID
    },
    title: {
      type: GraphQLString
    },
    description: {
      type: GraphQLString
    },
    address: {
      type: GraphQLString
    },
    location: {
      type: Location
    },
    photos: {
      type: new GraphQLList(GraphQLString),
      resolve: ({ photos, location }) => {
        location.latitude && location.longitude && photos.push(buildStaticMapUri(location))
        return photos
      }
    },
    date: {
      type: BarrierDate
    },
    author: {
      type: GraphQLUser,
      resolve: async ({ author }) => await User.findOne({ _id: author })
    },
    approves: {
      type: Approves
    }
  }
})