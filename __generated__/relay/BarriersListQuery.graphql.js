/**
 * @flow
 * @relayHash 02fc127df23f05d808956c17b3b8a886
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type BarriersListQueryVariables = {||};
export type BarriersListQueryResponse = {|
  +barriers: ?$ReadOnlyArray<?{|
    +id: ?string,
    +title: ?string,
    +description: ?string,
    +address: ?string,
    +location: ?{|
      +latitude: ?number,
      +longitude: ?number,
    |},
    +photos: ?$ReadOnlyArray<?string>,
    +approves: ?{|
      +count: ?number,
      +approvedByCurrentUser: ?boolean,
    |},
    +author: ?{|
      +id: ?string,
      +fullName: ?string,
    |},
    +date: ?{|
      +publish: ?any,
      +resolved: ?any,
    |},
  |}>
|};
export type BarriersListQuery = {|
  variables: BarriersListQueryVariables,
  response: BarriersListQueryResponse,
|};
*/


/*
query BarriersListQuery {
  barriers(range: [0, 20]) {
    id
    title
    description
    address
    location {
      latitude
      longitude
    }
    photos
    approves {
      count
      approvedByCurrentUser
    }
    author {
      id
      fullName
    }
    date {
      publish
      resolved
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "barriers",
    "storageKey": "barriers(range:[0,20])",
    "args": [
      {
        "kind": "Literal",
        "name": "range",
        "value": [
          0,
          20
        ]
      }
    ],
    "concreteType": "Barrier",
    "plural": true,
    "selections": [
      (v0/*: any*/),
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "title",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "description",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "address",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "location",
        "storageKey": null,
        "args": null,
        "concreteType": "Location",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "latitude",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "longitude",
            "args": null,
            "storageKey": null
          }
        ]
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "photos",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "approves",
        "storageKey": null,
        "args": null,
        "concreteType": "Approves",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "count",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "approvedByCurrentUser",
            "args": null,
            "storageKey": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "author",
        "storageKey": null,
        "args": null,
        "concreteType": "User",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "fullName",
            "args": null,
            "storageKey": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "date",
        "storageKey": null,
        "args": null,
        "concreteType": "BarrierDate",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "publish",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "resolved",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "BarriersListQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "BarriersListQuery",
    "argumentDefinitions": [],
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "BarriersListQuery",
    "id": null,
    "text": "query BarriersListQuery {\n  barriers(range: [0, 20]) {\n    id\n    title\n    description\n    address\n    location {\n      latitude\n      longitude\n    }\n    photos\n    approves {\n      count\n      approvedByCurrentUser\n    }\n    author {\n      id\n      fullName\n    }\n    date {\n      publish\n      resolved\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'ac20fc93ff6a6385bcf3cd2d17ce7733';
module.exports = node;
