/**
 * @flow
 * @relayHash e4ee00a13d2884e669069f9c1b0625c9
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CurrentUserQueryVariables = {||};
export type CurrentUserQueryResponse = {|
  +currentUser: ?{|
    +id: ?string,
    +name: ?string,
  |}
|};
export type CurrentUserQuery = {|
  variables: CurrentUserQueryVariables,
  response: CurrentUserQueryResponse,
|};
*/


/*
query CurrentUserQuery {
  currentUser: user {
    id
    name
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LinkedField",
    "alias": "currentUser",
    "name": "user",
    "storageKey": null,
    "args": null,
    "concreteType": "User",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "id",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "name",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "CurrentUserQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "CurrentUserQuery",
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "CurrentUserQuery",
    "id": null,
    "text": "query CurrentUserQuery {\n  currentUser: user {\n    id\n    name\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'e3d8340ef3e9396c264e77c007266e0b';
module.exports = node;
