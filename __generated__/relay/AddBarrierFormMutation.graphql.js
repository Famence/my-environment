/**
 * @flow
 * @relayHash 9609a03584d90a844c7d4d11ff862ce2
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type AddBarrierFormMutationVariables = {|
  title?: ?string,
  description: string,
  address: string,
|};
export type AddBarrierFormMutationResponse = {|
  +addBarrier: ?{|
    +status: ?string
  |}
|};
export type AddBarrierFormMutation = {|
  variables: AddBarrierFormMutationVariables,
  response: AddBarrierFormMutationResponse,
|};
*/


/*
mutation AddBarrierFormMutation(
  $title: String
  $description: String!
  $address: String!
) {
  addBarrier(title: $title, description: $description, address: $address) {
    status
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "title",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "description",
    "type": "String!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "address",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "addBarrier",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "address",
        "variableName": "address"
      },
      {
        "kind": "Variable",
        "name": "description",
        "variableName": "description"
      },
      {
        "kind": "Variable",
        "name": "title",
        "variableName": "title"
      }
    ],
    "concreteType": "AddBarrierMutationResult",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "status",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "AddBarrierFormMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "AddBarrierFormMutation",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "mutation",
    "name": "AddBarrierFormMutation",
    "id": null,
    "text": "mutation AddBarrierFormMutation(\n  $title: String\n  $description: String!\n  $address: String!\n) {\n  addBarrier(title: $title, description: $description, address: $address) {\n    status\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '4555552c215ca0d038caa2e5e2e94864';
module.exports = node;
